//Creamos los módulos que vamos a usar 
var incidenciasServices = angular.module('incidenciasServices', ['ngResource']); //Iniciamos el módulo cargando la dependencia que vamos a usar

incidenciasServices.factory('Incidencias',['$resource', 
	function($resource){
		var baseUrl = 'http://restapi.sergiosoriano.com/api/incidencias/:incidencias_id';
		return $resource(baseUrl,null, {
			getAll : {method: 'GET', isArray: true},
			update :  {method: 'PUT'},
			create : {method: 'POST'},
			remove : {method: 'DELETE'}	
	});
}]);