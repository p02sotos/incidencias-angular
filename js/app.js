'use strict';

/* App Module */

var incidenciasApp = angular.module('incidenciasApp', [
  'ngRoute', //Enturador
  'incidenciasControllers', //Controladores
  'incidenciasFilters', //Filtros
  'incidenciasServices',
   'ui.bootstrap' //Servicios

]);

incidenciasApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/incidencias', {
        templateUrl: 'templates/incidencias.html',
        controller: 'IncidenciasListController'
      }).
      when('/incidencias/new', {
        templateUrl: 'templates/incidencia.html',
        controller: 'NewIncidenciaController'
      }).
      when('/incidencias/:incidencias_id', {
        templateUrl: 'templates/incidencia.html',
        controller: 'IncidenciasDetailController'
      }).
      when('/',{
      	 templateUrl: 'templates/main.html',
        controller: 'MainCtrl'

      }).
      otherwise({
        redirectTo: '/'
      });
  }]);
incidenciasApp.config(function ($httpProvider) {
	$httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    });