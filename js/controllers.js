//Creamos los módulos que vamos a usar 
var incidenciasControllers = angular.module('incidenciasControllers', []);

incidenciasControllers.controller('IncidenciasListController', ['$scope','$location' ,'Incidencias',
	function($scope,$location, Incidencias) { 	
    	$scope.incidencias = Incidencias.getAll(); 
    	$scope.newIncidencia = false;
    	$scope.createIncidencia = function(){
    		$scope.newIncidencia = true;
    		$location.path('/incidencias/new');
    	} 
    	$scope.delete = function(incidenciaId){
    		alert('borrar ' + incidenciaId);
    		//Incidencias.remove({id:incidencia.id});
    	} 
    	$scope.view = function(incidenciaId){
    		
    		$location.path('/incidencias/' + incidenciaId);
    	} 
    	 
  	}  	
]);

incidenciasControllers.controller('IncidenciasDetailController', ['$scope', '$routeParams', 'Incidencias', 
	function($scope,$routeParams,Incidencias){		
		$scope.incidencia = Incidencias.get({incidencias_id: $routeParams.incidencias_id},function(incidencia){
		});
		$scope.editing = false;

		$scope.edit = function(){
			if ($scope.editing==true){		
				$scope.editing=false;
			} else {
				$scope.editing=true;
			}
		}
		$scope.submit= function(){
    		Incidencias.update($scope.incidencia);
    	} 
}]);
incidenciasControllers.controller('NewIncidenciaController', ['$scope', 'Incidencias', 
	function($scope,Incidencias){	
		$scope.creating = true;
		var incidencia = {};//Pasamos un Objeto Vacío Probar si esto funciona o hay que pasarle los keys vacíos.
		/*var incidencia = {
			Comunidad:"", Origen:"", Motivo:"", 
			Descripcion:"", Resumen:"",FechaEntrada: "",
			HoraEntrada:"",FechaResolución:"",Urgencia:0,
		};*/
		$scope.incidencia= incidencia;
		$scope.submit= function(){
    		
    		Incidencias.create($scope.incidencia);
    	} 

		
}]);
incidenciasControllers.controller('MainCtrl', ['$scope', function ($scope) {
	
}]);

